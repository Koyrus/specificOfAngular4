import {Component, HostListener, Injectable, OnInit} from '@angular/core';
import {DataService} from "../login/dataService";

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
@Injectable()
export class DashComponent implements OnInit {

  constructor(private data : DataService) { }



  @HostListener('document:click', ['$event'])
  public documentClick(event: Event): void {
    if(event != null){
      console.log("clicked");
      this.data.sessionTimer = 1000;
    }
  }

  ngOnInit() {
    this.data.timerFunction()
  }

}
