import {Component, HostListener, Injectable, OnInit} from '@angular/core';
import {DataService} from "../login/dataService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
@Injectable()
export class UserComponent implements OnInit {

  constructor(private data : DataService , private router : Router) { }


  @HostListener('document:click', ['$event'])
  public documentClick(event: Event): void {
    if(event != null){
      console.log("clicked");
      this.data.sessionTimer = 1000;
    }
  }


  ngOnInit() {
    console.log("user");
    console.log(JSON.stringify(this.data.storage));

  }

  toOtherView(){
    this.router.navigate(['dashboard'])
  }
}

