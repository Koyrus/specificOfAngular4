import {Component, HostListener, Input, OnInit} from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {DataService} from "./dataService";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {



  public username : string;
  public password : string;
  public showErrorMessageAtLogin : boolean = false;
  public errorMessageAtLogin : string = "Wrong credentials";

  constructor(private router : Router , private data : DataService) { }

  ngOnInit() {
    console.log("login");
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.stateGo();
    }
  }




  stateGo(){
    if(this.username == '123' && this.password == '123'){
      this.data.timerFunction();
      this.data.storage = {
        "username": this.username,
        "password": this.password
      };
      this.router.navigate(["userComponent"]);
    } else {
      console.log("error");
      this.showErrorMessageAtLogin = true;
    }
  }
}
