import {Component, NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {DataService} from "./login/dataService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataService]
})
@NgModule({
  imports: [BrowserModule]
})

export class AppComponent{

  constructor() {

  }




}
