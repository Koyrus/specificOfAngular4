import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { UserComponent } from './user/user.component';
import {RouterModule} from "@angular/router";
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { DashComponent } from './dash/dash.component';



const routes = [
  {path: '', component: LoginComponent },
  {path : "userComponent",component : UserComponent },
  {path : "dashboard" , component : DashComponent}


];


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LoginComponent,
    DashComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
